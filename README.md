Dr. Rhodes has a long history of working with many physicians across various disciplines. The most notable and far reaching of Dr. Rhodes achievements is the development of a device that allows MRI scans to be taken while the patient is in the flexion or extension position. The process itself is known as the Rhodes Procedure.

Dr. Rhodes enjoys an excellent referral program with physicians:

The referring physician receives a report of findings on each and every patient.
A comprehensive follow-up treatment plan is established.
Any patient that is found to have non-musculoskeletal disorders is referred out for an allopathic evaluation.
Whether you're a doctor who has been referred by a colleague or have simply heard of Dr. Rhodes on your own, we invite you to learn more and have a look around.